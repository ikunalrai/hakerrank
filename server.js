// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');

var port = process.env.PORT || 3001;



var db = require('./server/config/db');

var db_url = process.env.DATABASE_URL || db.url;


mongoose.connect(db.url);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});
/*var pg = require('pg');

pg.connect(db_url, function(err, client) {
  if (err) throw err;
  console.log('Connected to postgres! Getting schemas...');

  client
      .query('SELECT table_schema,table_name FROM information_schema.tables;')
      .on('row', function(row) {
        console.log(JSON.stringify(row));
      });
});*/

app.use(bodyParser.json()); 
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 
app.use(bodyParser.urlencoded({ extended: true })); 
// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override')); 


// Routing
app.use(express.static(__dirname + '/client'));


app.use(express.static(__dirname + '/'));
// routes ==================================================
require('./server/routes')(app); // configure our routes





app.listen(port, function () {
  console.log('Server listening at port %d', port);
});



// expose app           
exports = module.exports = app;      