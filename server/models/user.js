var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
name: String,
email: {type: String, unique:true},
createdOn: Date,
modifiedOn: { type: Date, default: Date.now },
lastLogin: Date
});
// Build the User model


userSchema.statics = {

  /**
   * Load
   *
   * @param {Object} options
   * @param {Function} cb
   * @api private
   */

   get: function (id, callback) {
    this.findOne({'instagramId': id}, function(error, items){
	    callback(error, items);
	});
   },
   create: function (data, callback) {
   	var user = new this(data);
    user.save(callback);
   }
};


module.exports  = mongoose.model( 'User', userSchema );