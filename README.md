
# Sample App
###### Prerequest

* nodejs 
	<pre><code>sudo apt-get install nodejs</code></pre>
* npm
	<pre><code>sudo apt-get install npm</code></pre>
* bower
	<pre><code>sudo npm install -g bower</code></pre>
* gulp
	<pre><code>sudo npm install -g gulp</code></pre>

A simple chat demo for angularjs



And point your browser to `http://localhost:3001`. Optionally, specify
a port by supplying the `PORT` env variable.

## Features

 